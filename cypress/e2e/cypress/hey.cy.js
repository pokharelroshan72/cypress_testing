function performLogin(email, password) {
    cy.get(':nth-child(1) > .form-control').type(email);
    cy.get(':nth-child(2) > .form-control').type(password);
    cy.get('.btn').click();
  }
  
  describe('Verify Login with valid and invalid credentials', () => {
    beforeEach(() => {
      cy.visit('https://next-realworld.vercel.app/user/login');
    });
  
    // TESTCASE:1
    it('Should verify login with valid signin users', () => {
      const validEmail = "hlotest00@gmail.com";
      const validPassword = "test";
  
      performLogin(validEmail, validPassword);
  
      cy.get(':nth-child(3) > .nav-link').contains('Settings').click();
      cy.wait(2000);
      cy.get('.btn-outline-danger').contains('click here to logout.').click();
      cy.wait(2000);
    });
  
    // TESTCASE:2
    it('Should verify login with invalid signin users', () => {
      const invalidEmail = "yyy@gmail.com";
      const invalidPassword = "password";
  
      performLogin(invalidEmail, invalidPassword);
  
      cy.get('.error-messages > li').should('be.visible');
      cy.wait(2000);
    });
  
    // TESTCASE:3
    it('Should validate with empty email', () => {
      const emptyEmail = "";
      const validPassword = "test";
  
      performLogin(emptyEmail, validPassword);
  
      cy.get('.error-messages > li').should('be.visible');
      cy.wait(2000);
    });
  
    // TESTCASE:4
    it('Should verify with empty password', () => {
      const validEmail = "hlotest00@gmail.com";
      const emptyPassword = "";
  
      performLogin(validEmail, emptyPassword);
  
      cy.get('.error-messages > li').should('be.visible');
    });
  });
  